# Finkraft.ai assignment

## Installation process

### Pre-requisites
Please ensure python-3.9.18 is available on the system or docker(version 24) is installed.

### To build from source
To build your own image:

#### Using Docker
There are 2 Dockerfiles for non-prod(./docker/Dockerfile.dev) and prod(./docker/Dockerfile.prod) environments. Non-prod runs the in-built flask server and Prod uses the Gunicorn server.

```
docker build -t <IMAGE_NAME>:<TAG> -f ./docker/<DOCKERFILE> .
```
To run this:

```
docker run -p8080:8080 <IMAGE_NAME>:<TAG>       
```

#### Without Docker

```
pip install -r requirements.txt
```
Set the env variable ```APP_ENV=DEV``` for development and ```APP_ENV=PROD```

To run the application
```
python main.py
```

### Automated Builds
In the repository's Settings > CI/CD > Variables, Set the below variables and their appropriate value. 
```
DOCKER_HUB_PASSWORD
DOCKER_HUB_USER
DOCKER_HUB_REPOSITORY
AWS_ACCESS_KEY_ID
AWS_DEFAULT_REGION
AWS_SECRET_ACCESS_KEY
CI_AWS_ECS_CLUSTER
CI_AWS_ECS_SERVICE_DEV
CI_AWS_ECS_SERVICE_PROD
```


