def test_request_hello(client):
    response = client.get("/")
    assert b"Hello, World!" in response.data


def test_request_hello_wrong_type(client):
    response = client.post("/")
    assert 405 == response.status_code
