from flask import Flask
import os
flask_app = Flask(__name__)


@flask_app.route('/')
def hello():
    return "Hello, World!"


if __name__ == '__main__':
    if os.environ["APP_ENV"] == "PROD":
        debug = False
    else:
        debug = True
    flask_app.run(debug=debug, host='0.0.0.0', port=8080)
